# === extends === #
extends Node

# === signals === #


# === enums === #

# === dictionaries === #


# === onready variables === #

# === export variables === #

# === constants and variables === #
var settings_file: = "user://Settings.save"
var music_enabled: = true


# === _init() and _ready() === #
func _ready() -> void:
	load_settings()


# === functions === #
func load_settings():
	var file = File.new()
	if file.file_exists(settings_file):
		file.open(settings_file, File.READ)
		music_enabled = file.get_var()
		file.close()

func save_settings():
	var file = File.new()
	file.open(settings_file, File.WRITE)
	file.store_var(music_enabled)
	file.close()
