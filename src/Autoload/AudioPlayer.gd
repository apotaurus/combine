# === extends === #
extends Node

# === signals === #


# === enums === #

# === dictionaries === #


# === onready variables === #
onready var player: = $Player
onready var main_music: = preload("res://asset/audio/Looking for a new beginning.ogg")

# === export variables === #

# === constants and variables === #


# === _init() and _ready() === #
func _ready() -> void:
	if Settings.music_enabled:
		play_music(main_music)


# === functions === #
func play_music(audio: AudioStream = main_music) -> void:
	print_debug("set audio")
	player.set_stream(audio)
	player.volume_db = 0
	player.play()

func fade_music() -> void:
	print_debug("fade music")
	$MusicFade.interpolate_property(player, "volume_db",
			0, -50, 1.0, Tween.TRANS_SINE, Tween.EASE_IN)
	$MusicFade.start()
	yield($MusicFade, "tween_all_completed")
	print_debug("yield")
	player.stop()

func stop_music() -> void:
	player.stop()

func toggle_music(play: bool) -> void:
	if play:
		play_music()
	else:
		player.stop()
