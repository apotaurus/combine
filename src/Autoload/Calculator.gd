# === extends === #
extends Node

# === signals === #
signal number_picked
signal victory


# === enums === #

# === dictionaries and arrays === #

# === onready variables === #

# === export variables === #

# === constants and variables === #
var expected: = 0
var total: = 0
var number: = 0 setget set_number
var current_operator: String setget set_operator


# === _init() and _ready() === #


# === functions === #
func reset() -> void:
	expected = 0
	total = 0
	number = 0

func set_number(new_number: int) -> void:
	number = new_number
	calculate()
	emit_signal("number_picked", current_operator)

func set_operator(new_operator: String) -> void:
	current_operator = new_operator

func calculate() -> void:
	if total == 0:
		total += number
	else:
		match current_operator:
			"+":
				total += number
			"-":
				total -= number
			"*":
				total *= number
			"/":
				total /= number
	check_victory()

func check_victory() -> void:
	if total == expected:
		emit_signal("victory")
