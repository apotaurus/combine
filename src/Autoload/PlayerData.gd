# === extends === #
extends Node

# === signals === #


# === enums === #
var COLORS = [
	"#5e315b",
	"#8c3f5d",
	"#ba6156",
	"#f2a65e",
	"#ffe478",
	"#cfff70",
	"#8fde5d",
	"#3ca370",
	"#3d6e70",
	"#4da6ff",
	"#66ffe3",
	"#e36956",
]
var USED_COLORS = []
var levels_completed = {
	"Level1": false,
	"Level2": false,
	"Level3": false,
	"Level4": false,
	"Level5": false,
	"Level6": false,
	"Level7": false,
	"Level8": false,
	"Level9": false,
	"Level10": false,
}

# === dictionaries === #


# === onready variables === #

# === export variables === #

# === constants and variables === #
var data_file = "user://PlayerData.save"
var total_blocks: int
var numbers_picked: = 0 setget set_picked
var level


# === _init() and _ready() === #
func _ready() -> void:
	load_data()


# === functions === #
func reset() -> void:
	COLORS = COLORS + USED_COLORS
	USED_COLORS = []
	numbers_picked = 0

func load_level(_level: Object) -> void:
	level = _level
	total_blocks = level.get_node("Numbers").get_child_count()

func set_picked(num_picked: int) -> void:
	numbers_picked = num_picked
	check_defeat()

func check_defeat() -> void:
#	if (total_blocks - numbers_picked) == 0 and Calculator.total != Calculator.expected:
#		var main_screen: PackedScene = load("res://src/Screens/MainScreen.tscn")
#		get_tree().change_scene_to(main_screen)
	pass

func load_data() -> void:
	var file = File.new()
	if file.file_exists(data_file):
		file.open(data_file, File.READ)
		levels_completed = file.get_var()
		file.close()

func save_data():
	var file = File.new()
	file.open(data_file, File.WRITE)
	file.store_var(levels_completed)
	file.close()
