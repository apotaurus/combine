# class name and extends
tool
class_name EndPortal
extends Area2D

# signals


# onready variables

# enums

# export variables
export var next_level: PackedScene

# constants and variables


# _init() and _ready()


# _process() and _physics_process()
func _physics_process(delta: float) -> void:
#	rotation += delta
	pass


# _input() and _unhandled_input()


# functions
func _get_configuration_warning() -> String:
	return "The property Next Level can't be empty" if not next_level else ""

func go_to_next_level() -> void:
	get_tree().paused = true
#	animation
	get_tree().paused = false
	var level: = get_parent().get_name()
	PlayerData.levels_completed[level] = true
	PlayerData.save_data()
	get_tree().change_scene_to(next_level)

#signal functions
func _on_area_entered(_area: Area2D):
	if Calculator.total == Calculator.expected:
		go_to_next_level()
