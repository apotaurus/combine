# class name and extends
extends Area2D

# signals
signal color_picked


# onready variables
onready var timer: Timer = $SwipeTimeout
export(float, 1.0, 1.5) var max_diagonal_slope

# enums

# export variables
export(int) var SPEED

# constants and variables
var direction: = Vector2.ZERO
var block: Area2D
var is_start: = true
var swipe_start_position: = Vector2.ZERO
var minimum_drag: = 100


# _init() and _ready()


# _process() and _physics_process()
func _physics_process(delta: float) -> void:
	if not block:
		position += SPEED * delta * direction
	else:
		position = position.linear_interpolate(block.position, delta * 4.0)


# _input() and _unhandled_input()
func _unhandled_input(event: InputEvent) -> void:
	if block or is_start:
		if event is InputEventScreenTouch:
			if event.is_pressed():
				_start_detection(event.position)
			elif not timer.is_stopped():
				var swipe_direction: = _end_detection(event.position)
				direction = check_direction(swipe_direction)
				if direction != Vector2.ZERO:
					block = null
		is_start = false


# functions
func _start_detection(position: Vector2) -> void:
	swipe_start_position = position
	timer.start()

func _end_detection(position: Vector2) -> Vector2:
	timer.stop()
	var _direction: Vector2 = (position - swipe_start_position).normalized()
	# Swipe angle is too steep
	if abs(_direction.x) + abs(_direction.y) >= max_diagonal_slope:
		return Vector2.ZERO

	var swipe_direction: = Vector2.ZERO
	if abs(_direction.x) > abs(_direction.y):
		swipe_direction = Vector2(sign(_direction.x), 0.0)
	else:
		swipe_direction = Vector2(0.0, sign(_direction.y))
	return swipe_direction

func check_direction(_swipe: Vector2) -> Vector2:
	var swipe: = Vector2.ZERO
	match _swipe:
		Vector2.UP:
			swipe = _swipe if (block.directions["UP"] == true) else Vector2.ZERO
		Vector2.DOWN:
			swipe = _swipe if (block.directions["DOWN"] == true) else Vector2.ZERO
		Vector2.RIGHT:
			swipe = _swipe if (block.directions["RIGHT"] == true) else Vector2.ZERO
		Vector2.LEFT:
			swipe = _swipe if (block.directions["LEFT"] == true) else Vector2.ZERO
	return swipe

func _end_level() -> void:
	pass


#signal functions
func _on_area_entered(area: Area2D) -> void:
	block = area 
	if block is CellNumber and block.remaining_touches >= 0:
		emit_signal("color_picked", block.color_value)
		Calculator.number = block.number
#		Calculator.number = int(block.get_node("Sprite/Label").text)
	elif block is EndPortal:
		$AnimationPlayer.play("ended")

func _on_SwipeTimeout_timeout() -> void:
	emit_signal('swipe_canceled', swipe_start_position)
