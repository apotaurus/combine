# class name and extends
tool
class_name Block
extends Area2D

# signals


# onready variables

# enums

# export variables
export var directions = {
	"UP": false,
	"DOWN": false,
	"RIGHT": false,
	"LEFT": false,
}

# constants and variables


# _init() and _ready()
func _ready() -> void:
	for direction in directions:
		if directions[direction] == false:
			match direction:
				"UP":
					$Directions/UP.visible = false
				"DOWN":
					$Directions/DOWN.visible = false
				"RIGHT":
					$Directions/RIGHT.visible = false
				"LEFT":
					$Directions/LEFT.visible = false


# _process() and _physics_process()


# _input() and _unhandled_input()


# functions


#signal functions
