# class name and extends
class_name CellNumber
extends Block

# signals


# onready variables

# enums


# export variables
export(int) var number
export(int) var remaining_touches

# constants and variables
var color_value: String

# _initialize() and _ready()
func _ready() -> void:
	randomize()
	_initialize()

func _initialize():
#	number = randi() % 10 + 1
	$Sprite/Label.text = str(number)
#	show_operators()

# _process() and _physics_process()


# _input() and _unhandled_input()


# functions
func modulate_directions() -> void:
	for direction in $Directions.get_children():
		direction.modulate = Color(10,10,10)

#signal functions
func _on_area_entered(_area: Area2D) -> void:
	remaining_touches -= 1
	if remaining_touches >= 0:
		PlayerData.numbers_picked += 1
		var index: int = randi() % PlayerData.COLORS.size()
		color_value = PlayerData.COLORS[index]
		PlayerData.COLORS.erase(color_value)
		PlayerData.USED_COLORS.append(color_value)
		modulate = Color(color_value)

func _on_area_exited(_area: Area2D) -> void:
	modulate_directions()
	if remaining_touches >= 0:
		$AnimationPlayer.play("shrink")
