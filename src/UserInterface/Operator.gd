# class name and extends
extends TextureButton

# signals


# onready variables

# enums

# export variables
export(String) var symbol

# constants and variables


# _init() and _ready()
func _ready() -> void:
	$Label.text = symbol


# _process() and _physics_process()


# _input() and _unhandled_input()


# functions


#signal functions
func _on_button_up():
	for operator in get_tree().get_nodes_in_group("operators"):
		operator.modulate = Color("#ffffff")
	modulate = Color("#66ffe3")
	Calculator.current_operator = symbol
