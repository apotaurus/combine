tool
extends TextureButton


export(String, FILE) var next_scene: = ""


func _get_configuration_warning() -> String:
	return "The property Next Scene can't be empty" if next_scene == "" else ""


func _on_button_up() -> void:
	PlayerData.reset()
	get_tree().change_scene(next_scene)
