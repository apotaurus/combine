extends "res://src/UserInterface/TextureSceneChangeButton.gd"

export(Color) var color_completed


func _ready() -> void:
	var levels: Dictionary = PlayerData.levels_completed
	for level in levels:
		if level == get_name() and levels[level] == true:
			modulate = Color(color_completed)


func _on_button_up() -> void:
#	AudioPlayer.fade_music()
	var music: = load("res://asset/audio/Resilience.ogg")
	yield(AudioPlayer.fade_music(), "completed")
	print_debug("pronto a cambiare musica")
	if Settings.music_enabled:
		AudioPlayer.play_music(music)
	._on_button_up()
