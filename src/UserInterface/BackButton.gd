tool
extends "res://src/UserInterface/TextureSceneChangeButton.gd"


func _on_button_up() -> void:
#	AudioPlayer.fade_music()
	var music: = load("res://asset/audio/Looking for a new beginning.ogg")
	yield(AudioPlayer.fade_music(), "completed")
	print_debug("pronto a cambiare musica")
	if Settings.music_enabled:
		AudioPlayer.play_music(music)
	._on_button_up()

