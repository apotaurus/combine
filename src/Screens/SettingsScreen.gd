# class name and extends
extends Control

# signals


# onready variables
onready var music_button: = $VBoxContainer/MusicButton

# enums

# export variables

# constants and variables
var music_textures = {true: preload("res://asset/images/musical note.png"),
					false: preload("res://asset/images/musical note_greyscale.png")}


# _init() and _ready()
func _ready() -> void:
	setup_buttons()


# _process() and _physics_process()


# _input() and _unhandled_input()


# functions
func setup_buttons() -> void:
	music_button.texture_normal = music_textures[Settings.music_enabled]


#signal functions
func _on_MusicButton_button_up():
	Settings.music_enabled = !Settings.music_enabled
	music_button.texture_normal = music_textures[Settings.music_enabled]
	Settings.save_settings()
	AudioPlayer.toggle_music(Settings.music_enabled)
