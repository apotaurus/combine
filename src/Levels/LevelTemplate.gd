# class name and extends
tool
extends Node2D

# signals


# onready variables
onready var Player: = preload("res://src/Actors/Player.tscn")
onready var numbers_label: = $HUD/Userinterface/HBoxContainer/NumbersLabel
onready var result_label: = $HUD/Userinterface/HBoxContainer/ResultLabel
onready var anim_player: = $HUD/Userinterface/AnimationPlayer

# enums

# export variables
export(int) var total
export var operators = {
	"PLUS": false,
	"MINUS": false,
	"MULT": false,
	"DIV": false,
}

# constants and variables
var bbcolor: String
var is_first_number: = true


# _init() and _ready()
func _ready() -> void:
	PlayerData.reset()
	PlayerData.load_level(self)
	Calculator.reset()
	
	for operator in operators:
		if operators[operator] == false:
			match operator:
				"PLUS":
					$HUD/Userinterface/VBoxContainer/PLUS.visible = false
				"MINUS":
					$HUD/Userinterface/VBoxContainer/MINUS.visible = false
				"MULT":
					$HUD/Userinterface/VBoxContainer/MULT.visible = false
				"DIV":
					$HUD/Userinterface/VBoxContainer/DIV.visible = false

	var start_position: Vector2 = $StartCell.position
	var player: = Player.instance()
	add_child(player)
	player.set_position(start_position)
	player.scale = Vector2(0.5, 0.5)
	##############################################
	Calculator.current_operator = "+"
	$HUD/Userinterface/VBoxContainer/PLUS.modulate = Color("#66ffe3")
	##############################################
	Calculator.expected = total
	$EndPortal/StartSprite/ExpectedLabel.text = str(total)
	numbers_label.push_align(RichTextLabel.ALIGN_CENTER)
	
	Calculator.connect("number_picked", self, "number_picked")
	Calculator.connect("victory", self, "show_victory")
	player.connect("color_picked", self, "set_bbcolor")

# _process() and _physics_process()


# _input() and _unhandled_input()


# functions
func _get_configuration_warning() -> String:
	return "The name must be the same of the level" if get_name() == "LevelTemplate" else ""

func show_numbers(operator: String) -> void:
	if not is_first_number:
		numbers_label.append_bbcode(" " + operator + " " + 
			"[color="+bbcolor+"]" + str(Calculator.number) + "[/color]")
	else:
		numbers_label.append_bbcode("[color="+bbcolor+"]" + str(Calculator.number) + "[/color]")
		is_first_number = false
	anim_player.play("picked")

func show_result() -> void:
	result_label.bbcode_text = "[center]" + str(Calculator.total) + "[/center]"

# signal functions
func set_bbcolor(color: String) -> void:
	bbcolor = color

func number_picked(operator: String) -> void:
	show_numbers(operator)
	show_result()

func show_victory() -> void:
	$EndPortal/AnimationPlayer.play("unlock")

func _on_ResultLabel_gui_input(event: InputEvent) -> void:
	if event is InputEventScreenTouch and event.is_pressed():
		anim_player.play("picked")
