# class name and extends
extends "res://src/Levels/LevelTemplate.gd"

# signals


# onready variables
onready var countdown_label: = $HUD/Userinterface/CountdownlLabel

# enums

# export variables
export(int) var countdown

# constants and variables


# _init() and _ready()
func _ready():

	countdown_label.text = str(countdown)


# _process() and _physics_process()


# _input() and _unhandled_input()


# functions

#signal functions
func _on_Countdown_timeout():
	countdown -= 1
	if countdown == 15:
		countdown_label.modulate = Color("#f2a65e")
	elif countdown == 10:
		countdown_label.modulate = Color("#e78686")
	elif countdown == 0:
		get_tree().reload_current_scene()
	countdown_label.text = str(countdown)
